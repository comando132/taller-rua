# Práctica final

Deben usar Livewire, Eloquent, Diseño basado en dominio con actions, query builders y dto's.
Aplicar TailwindCss. AlpineJS solamente si lo requieren y encuentran casos donde pueda ayudar.

## Registro de recurso (parcial)
  
  Desarrollar la funcionalidad para el registro de recursos. Los campos de registro son:
  
  - Título: campo de texto, máx caracteres (elemento.elem_nombre)
  - Descripción: textarea, máx caracteres (elemento.elem_descripcion)
  - Palabras clave: se pueden agregar hasta 5 palabras clave, obligatoria al menos una, , máx caracteres. (tabla palabra_clave)
  - Formato: lista desplegable, obligatorio (recurso.formato_id)
  - Categoría: lista desplegable, obligatorio (recurso.categoria_id)
  - Estado de publicacion: radio botón con las opciones Publicado / No publicado, obligatorio. En la base se guarda como "S" o "N" (elemento.elem_estatus_publicacion)

Consideraciones:

  - Al registrar, el estado general (elem_estado) se debe guardar como "1"
  - Debe guardarse tambien el campo de elem_fecha_creacion con la fecha y ahora actual
  - Si el estado de publicacion se asignó a "S" (Publicado), debe guardarse el campo elem_fecha_publicacion
  - Usar transacción.
    
## Listado de recursos con criterios de búsqueda

  Desarrollar un componente para presentar la lista de recursos, la cual deberá tener criterios de búsqueda, ordenamiento y paginación.

  Columnas:
  - Id
  - Título
  - Descripción
  - Categoría
  - Formato
  - Estado de publicación ("Publicado / No publicado")
  - Fecha de publicación (elem_fecha_publicacion). En formato dd/mm/aaaa
  - Fecha de registro (elem_fecha_creacion). En formato dd/mm/aaaa
    
  Criterios de búsqueda:
  - Texto: buscar dentro de titulo y descripcion. 
    - Opcional: que se busquen las palabras separadas, es decir si ingreso el texto "computación cuántica", me debería encontrar recurso que tengan ambas palabras aunque no esten juntas.
  - Categoria: lista desplegable
  - Formato: lista desplegable
  - Estado de publicación ("Publicado / No publicado"): radioboton
    
  Consideraciones:
  - No se deben mostrar los recursos "eliminados", es decir aquellos con elem_estatus = "0"
  - Boton para editar: Debe presentar el formulario de registro prellenado, aplican las mismas reglas.
  - Boton para eliminar (es una baja logica, debe cambiarse el estatus del elemento  (elem_estado a "0"))