# Instalar Jetstream con Livewire 



## Instalar Jetstream con Livewire

- [ ] En una terminal ejecutar:

```
sail composer require laravel/jetstream
sail php artisan jetstream:install livewire
sail php artisan migrate
sail php artisan vendor:publish --tag=jetstream-views
```

## Configuración Vite

- [ ] Editar el archivo package.json y agregar lo siguiente en la sección scripts:
```
"watch": "vite build --watch"
```

- [ ] Editar el archivo vite.config.js:
```
import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
            ],
            refresh: [
                ...refreshPaths,
                'resources/routes/**',
                'routes/**',
                'resources/views/**',
                'app/Http/Livewire/**',
            ],
        }),
    ],
    server: {
        host: '0.0.0.0',
        hmr: {
            host: 'localhost',
        },
        watch: {
            usePolling: true,
        }
    }

});
```

- [ ] Borrar cache de sail e iniciar el servicio nuevamente. Esto sirve para "Hot Reload" es decir que cuando se hace un cambio en codigo se actualiza automáticamente el navegador.

```
sail build --no-cache
sail up
sail npm run dev
```