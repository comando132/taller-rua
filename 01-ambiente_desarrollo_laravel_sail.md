# Guía para crear un ambiente de desarrollo con [Laravel Sail](https://laravel.com/docs/9.x/sail)



## (Para WindowsOS) WSL2 (Windows Subsystem for Linux)

- [ ] [Instalar y habilitar WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

En una terminal ejecutar:

```
wsl --install
```

- [ ] Después de instalar Ubuntu, se solicita crear un usuario, se recomienda usar los datos:

> Usuario: proyectos

> Password: proyectos

La carpeta home del usuario será: /home/proyectos

## Docker Desktop

- [ ] [Instalar Docker Desktop](https://www.docker.com/products/docker-desktop/)
- [ ] Seleccionar la opción de WSL2 cuando se solicite o bien posteriormente se puede habilitar entrando al menú de configuración y seleccionando la opción "Use the WSL2 based engine" (https://docs.docker.com/docker-for-windows/wsl/) 

## Visual Studio Code

- [ ] [Instalar Visual Studio Code](https://code.visualstudio.com/)
- [ ] [Instalar el paquete de extensiones para VSCode para desarrollo remoto](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- [ ] En VSCode, seleccionar la opcion "New WSL Window", esto abrirá una nueva ventana.
- [ ] Abrir la terminal dentro de VSCode (Ctrl+ñ) o bien View->Terminal
- [ ] En la terminal: Agregar el usuario de WSL al grupo de Docker (ya que por default Docker solo está asociado al usuario root de Ubuntu)

```
sudo usermod -aG docker ${USER}
```
- [ ] Cerrar la ventana de VSCode y abrir una nueva (para que tome efectos los cambios realizados)
- [ ] Abrir la terminal dentro de VSCode (Ctrl+ñ) o bien View->Terminal

## Proyecto Laravel

- [ ] En la terminal: Crear un proyecto de laravel con el nombre "proyecto1". Se agregarán las herramientas Postgres, Meilisearch, Selenium y Redis

```
curl -s "https://laravel.build/proyecto1?with=pgsql,meilisearch,selenium,redis" | bash
```

- [ ] Esperar a que se descarguen todos los paquetes, este proceso puede tardar varios minutos (15 a 30 minutos)
- [ ] En la terminal: Agregar permanentemente el comando sail:

```
vi ~/.bashrc
```
Agregar la siguiente linea al final del archivo; moverse al final del archivo, presionar la tecla i y pegar la linea; al finalizar presionar la tecla Esc y escribir :wq para guardar y salir:

```
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```
- [ ] En la terminal: Moverse a la carpeta proyecto1 e ingresar el comando para abrir una nueva ventana de VSCode

```
cd proyecto1
code .
```
- [ ] Abrir la terminal de la nueva ventana
- [ ] En la terminal: Levantar el proyecto. Se levantan los servicios requeridos para el funcionamiento del proyecto como es el php, servidor web y bases de datos

```
sail up -d
```

- [ ] Llegado a este punto ya se puede ver el proyecto en el navegador en http://localhost
