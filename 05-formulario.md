# Formulario para crear usuario

## Clase App/Http/Livewire/Formulario.php
- [ ] Crear un nuevo componente livewire "Formulario"
- [ ] Agregar los atributos públicos $name, $email y $password
- [ ] Especificar el layout "guest" en el método render
- [ ] Agregar el atributo $rules con las reglas de validación de los 3 campos
- [ ] Para validar cada campo conforme se capturan, agregar el método "updated" el cual debe ejecutar la validación del campo actualizado, utilizando $this->validateOnly
- [ ] Agregar un método llamado "submit", el cual debe validar todos los campos  ($this->validate) y posteriormente guardar el registro en la base de datos utilizando Eloquent (User::create); tambien debe limpiar los campos (ver siguiente punto).
- [ ] Agregar un método llamado "limpiar", el cual debe limpiar los valores de los 3 atributos publicos

## Vista resources/views/livewire/formulario.blade.php
- [ ] Agregar un formulario html con campos para los 3 atributos del componente (name, email y password). Los campos deben estar ligados a los atributos utilizando wire:model
- [ ] Después de cada campo agregar el mensaje de validación si existe (@error('name) {{ $message}} @enderror)
- [ ] La etiqueta form debe especificar que el enviarse o hacer submit, se debe llamar el método submit del componente (wire:submit.prevent)
- [ ] El botón Guardar debe ser del tipo "submit"
- [ ] Agregar un botón Limpiar el cual al darle clic debe llamar al método limpiar del componente (wire:click)

## Rutas routes/web.php
- [ ] Agregar una ruta en web.php para poder visualizar el componente (Route::get('/formulario', Formulario::class);)
